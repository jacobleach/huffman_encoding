module Main (main) where

import Test.Framework
import Test.HUnit
import Test.Framework.Providers.HUnit
import Test.Framework.Providers.QuickCheck2
import HuffmanTree
import Bit

main :: IO ()
main = defaultMain tests

tests = [
    --Test ==
    testCase "CharCount == test" 
        $ testCharCountEq (CharCount 'a' 1) (CharCount 'a' 1),
    
    --Test not . ==
    testCase "CharCount != test, different letter, same count" 
        $ testCharCountNotEq (CharCount 'b' 1) (CharCount 'a' 1),
    testCase "CharCount != test, same letter, different count"
        $ testCharCountNotEq (CharCount 'a' 2) (CharCount 'a' 1),
    testCase "CharCount != test, different letter, different count"
        $ testCharCountNotEq (CharCount 'b' 2) (CharCount 'a' 1),
    
    --Test <=
    testCase "CharCount <= test, same char, larger count" 
        $ testCharCountLT (CharCount 'a' 1) (CharCount 'a' 2),
    testCase "CharCount <= test, larger char, larger count" 
        $ testCharCountLT (CharCount 'a' 1) (CharCount 'b' 2),
    testCase "CharCount <= test, smaller char, larger count" 
        $ testCharCountLT (CharCount 'b' 1) (CharCount 'a' 2),
    testCase "CharCount <= test, larger char, same count" 
        $ testCharCountLT (CharCount 'a' 1) (CharCount 'b' 1),
    testCase "CharCount <= test, same char, same count" 
        $ testCharCountLT (CharCount 'a' 1) (CharCount 'a' 1),
    
    --Test >= 
    testCase "CharCount >= test, same char, larger count" 
        $ testCharCountGT (CharCount 'a' 2) (CharCount 'a' 1),
    testCase "CharCount >= test, larger char, larger count" 
        $ testCharCountGT (CharCount 'b' 2) (CharCount 'a' 1),
    testCase "CharCount >= test, smaller char, larger count" 
        $ testCharCountGT (CharCount 'a' 2) (CharCount 'b' 1),
    testCase "CharCount >= test, larger char, same count" 
        $ testCharCountGT (CharCount 'b' 1) (CharCount 'a' 1),
    testCase "CharCount >= test, same char, same count" 
        $ testCharCountGT (CharCount 'a' 1) (CharCount 'a' 1)
    ]

testCharCountEq :: CharCount -> CharCount -> Assertion
testCharCountEq a b = True @=? a == b

testCharCountNotEq :: CharCount -> CharCount -> Assertion
testCharCountNotEq a b = False @=? a == b

testCharCountLT :: CharCount -> CharCount -> Assertion
testCharCountLT a b = True @=? a <= b

testCharCountGT :: CharCount -> CharCount -> Assertion
testCharCountGT a b = True @=? a >= b
