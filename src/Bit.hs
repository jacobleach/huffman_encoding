module Bit
(
    Bit(Zero, One),
    Bits((.&.), (.|.), xor, complement, shift, rotate, bitSize, isSigned, testBit, bit, popCount)
) where

import Data.Bits

data Bit = Zero | One deriving (Show, Eq)

instance Bits Bit where
    (.&.) One  One  = One
    (.&.) _    _    = Zero

    (.|.) Zero Zero = Zero
    (.|.) _    _    = One

    (xor) Zero Zero = Zero
    (xor) One  One  = Zero
    (xor) _    _    = One

    complement Zero = One
    complement One  = One

    shift bit 0     = bit
    shift _ _       = Zero

    rotate bit _    = bit

    bitSize _       = 1

    isSigned _      = False

    testBit One  0  = True
    testBit _    _  = False

    bit 0           = Zero
    bit 1           = One

    popCount Zero   = 0
    popCount One    = 1
