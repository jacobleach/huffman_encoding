module BitString () where

import Data.Bits
import Bit

data CanonicalHuffmanCode = Code [Bit] deriving (Show, Eq) 

shift :: CanonicalHuffmanCode -> Int -> CanonicalHuffmanCode
shift code amount 
    | amount >  0 = shiftLeft
    | amount <  0 = shiftRight 
    | amount == 0 = code

shiftLeft :: CanonicalHuffmanCode -> Int -> CanonicalHuffmanCode
shiftLeft 

add (Code a) (Code b) 
    = Code $ removeZero (foldl (\b a -> (addPlace (head b) a) ++ (tail b)) [Zero] (reverse (zip a b)))

checkRemove :: [Bit] -> [Bit] -> [Bit]
checkRemove a b 
    | (head a) == Zero = b
    | otherwise        = removeZero b

removeZero :: [Bit] -> [Bit]
removeZero []        = []
removeZero [Zero]    = [Zero]
removeZero (Zero:xs) = xs
removeZero xs        = xs

addPlace :: Bit -> (Bit, Bit) -> [Bit]
addPlace Zero (Zero, Zero) = [Zero, Zero]
addPlace Zero (One,  Zero) = [Zero, One]
addPlace Zero (One,  One)  = [One, Zero]
addPlace Zero (Zero,  One)  = [Zero, One]
addPlace One  (One,  One)  = [One, One]
addPlace One  (Zero, Zero) = [Zero, One]
addPlace One  (One,  Zero) = [One, Zero]
addPlace One  (Zero, One)  = [One, Zero]
