module HuffmanTree 
(
    CharCount (CharCount), 
    Eq, 
    Ord
) 
where

import Data.List

data CharCount = CharCount { char :: Char, count :: Int } deriving (Show)

instance Eq CharCount where
    (CharCount a b) == (CharCount c d)
        | (a == c) && (b == d) = True
        | otherwise            = False

instance Ord CharCount where
    (CharCount a b) <= (CharCount c d)
        | (b < d)              = True
        | (b == d) && (a <= c) = True
        | otherwise            = False

data Node = Leaf { charCount :: CharCount  } | 
            Node { left :: Node, right :: Node, value :: Int } 

instance Show Node where
    show = showRec 6 

showRec indent (Leaf a)     = "Leaf:\n" ++ (getIndent indent) ++ "Char:  " ++ ((char a) : []) ++ "\n" ++ (getIndent indent) ++ "Count: " ++ (show (count a))
showRec indent (Node a b c) = "Node: " ++ (show c) ++ "\n" ++ (getIndent indent) ++ (showRec (indent + 6) a) ++ "\n" ++ (getIndent indent) ++ (showRec (indent + 6)  b)

getIndent :: Int -> String
getIndent a = replicate a ' '

instance Eq Node where
    (Leaf a)     == (Leaf b)     = (a == b)
    (Node a b c) == (Node d e f) = (a == d) && (b == e) && (c == f)
    _            == _            = False

instance Ord Node where
    (Leaf a)     <= (Leaf b)     = a <= b
    (Leaf a)     <= (Node _ _ b) = (count a) <= b
    (Node _ _ a) <= (Leaf b)     = a <= (count b)
    (Node _ _ a) <= (Node _ _ b) = a <= b

node :: Node -> Node -> Node
node a@(Leaf c)     b@(Leaf d)      = Node a b ((count c) + (count d))
node a@(Leaf c)     b@(Node _ _ _)  = Node a b ((count c) + (value b))
node a@(Node _ _ _) b@(Leaf c)      = Node a b ((value a) + (count c))
node a@(Node _ _ _) b@(Node _ _ _)  = Node a b ((value a) + (value b))

buildLeaves :: [CharCount] -> [Node]
buildLeaves a = map (Leaf) a

buildTree :: [CharCount] -> Node
buildTree charCounts = f (sort (buildLeaves charCounts))
    where f (x:[])   = x
          f (x:y:xs) = f (sort ((node x y) : xs))
